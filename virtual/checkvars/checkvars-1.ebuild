# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="Check readonly vars used by ebuilds"
SLOT="0"
KEYWORDS="alpha amd64 arm arm64 hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

IUSE=""

# mail-mta/citadel is from sunrise
#RDEPEND="|| (	mail-mta/nullmailer
#				mail-mta/msmtp[mta]
#				mail-mta/ssmtp[mta]
#				mail-mta/courier
#				mail-mta/esmtp
#				mail-mta/exim
#				mail-mta/mini-qmail
#				mail-mta/netqmail
#				mail-mta/postfix
#				mail-mta/qmail-ldap
#				mail-mta/sendmail
#				mail-mta/opensmtpd
#				mail-mta/citadel[-postfix] )"
RDEPEND=""

#src_configure() {
# echo "1"
#}

pre_src_install() {
#einfo "bla"
  ewarn "Variables"
  ewarn "'  \${P} (package name):' ${P}"
  ewarn "'  \${PN}              :' ${PN}"
  ewarn "'  \${PV}              :' ${PV}"
  ewarn "'  \${PR}              :' ${PR}"
  ewarn "'  \${PVR}             :' ${PVR}"
  ewarn "'  \${PF}              :' ${PF}"
  ewarn "'  \${A}               :' ${A}"
  ewarn "'  \${CATEGORY}        :' ${CATEGORY}"
  ewarn "'  \${FILESDIR}        :' ${FILESDIR}"
  ewarn "'  \${WORKDIR}         :' ${WORKDIR}"
  ewarn "'  \${T}               :' ${T}"
  ewarn "'  \${D}               :' ${D}"
  ewarn "'  \${HOME}            :' ${HOME}"
  ewarn "'  \${ROOT}            :' ${ROOT}"
  ewarn "'  \${DISTDIR}         :' ${DISTDIR}"
  ewarn "'  \${EPREFIX}         :' ${EPREFIX}"
  ewarn "'  \${ED}              :' ${ED}"
  ewarn "'  \${EROOT}           :' ${EROOT}"
  ewarn "'  \${SYSROOT}         :' ${SYSROOT}"
  ewarn "'  \${ESYSROOT}        :' ${ESYSROOT}"
  ewarn "'  \${BROOT}           :' ${BROOT}"
  ewarn ""
  ewarn "'  \${DEPEND}          :' ${DEPEND}"
#  ewarn "'  \${D}:' ${D}"
#  die "End" 2>&1 >/dev/null
#  exit
}

#exit

#src_install() {
# echo "2"
#  die >/dev/null
#}
