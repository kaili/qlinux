qLinux is an experimental project (study) based on Gentoo. Thus
the qLinux overlay provides additional tools and ebuilds - and
more important overlaying profiles.


The primary goals are:
  - KISS (keep it simple, stupid)
  - separate data from code (programs)
  - _slightly_ divergate from FHS
  - consolidate things which belongs together

