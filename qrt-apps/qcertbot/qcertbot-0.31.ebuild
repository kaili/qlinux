# Copyright @2019 Kai Peter
# Distributed under the terms of the ISC License

EAPI=7

inherit eutils

DESCRIPTION="A replacement for certbot-apache"
HOMEPAGE="http://gentoo.qware.org"
SRC_URI="http://ftp.dyndn.es/gentoo/${PN}-${PV}.tgz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="amd64 ~arm64"
IUSE=""

DEPEND="
    app-crypt/certbot
"

RDEPEND="${DEPEND}
	!app-crypt/certbot-apache
"
#	www-servers/apache

src_unpack() {
	cd ${WORKDIR}
	mkdir ${P}
	cd ${P}
	unpack ${A}
}

src_install() {
	dodir /var/local/bin
	insopts -m 755
	insinto /var/local/bin
    doins qcertbot
}

pkg_preinst() {
  # This is a really annoying situation by Gentoo path policies: it restricts the
  # locations where an ebuild can install files into.  By using a locatione which
  # isn't part of the policy, a QA warning will be printed (after 'src_install').
  #
  # Workaround: manipulate the files in '${D}' ('$PORTAGE_BUILDDIR}/image') right
  # __BEFORE__ they will be installed into the live filesystem.

  # Here we have used '/var' as a 'dummy for '/usr' (see '${MY_BIN_DIR}' above).
  mv ${D}/var ${D}/usr
}
