# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

#inherit autotools
#flag-o-matic toolchain-funcs
# eutils
# fixheadtails user

DESCRIPTION="The core utilities to handle the qLinux experiment"
HOMEPAGE="http://gentoo.qware.org"
SRC_URI="http://ftp.dyndn.es/gentoo/${P}.tgz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 amd64-qLinux"
IUSE="repos"

RESTRICT="QA_INSTALL_PATHS"

DEPEND="
	sys-kernel/genkernel
	sys-process/lsof
	repos? ( app-eselect/eselect-repository )
"

#RDEPEND="${DEPEND}"

post_src_unpack() {
	mv "${WORKDIR}/${PN}" "${WORKDIR}/${PN}-${PV}"
}

src_compile() {
	emake "-j1" || die
}

src_install() {

	dodir "/etc/portage/repo.postsync.d"

	QINSTALL="${S}/install"
	"${QINSTALL}"

	# prevent QA install warning 'unexpected install path' - "/var" is ok
	# always (we have to revert this in 'pkg_preinst')
	mv "${D}"/usr "${D%}"/var

	return
}

pkg_preinst() {
	# This is a really annoying situation by Gentoo path policies: it restricts the
	# locations where an ebuild can install files into.  By using a locatione which
	# isn't part of the policy, a QA warning will be printed (after 'src_install').
	#
	# Workaround: manipulate the files in '${D}' ('$PORTAGE_BUILDDIR}/image') right
	# __BEFORE__ they will be installed into the live filesystem.

	# Here we have used '/var' as a 'dummy for '/usr' (see '${MY_BIN_DIR}' above).
	mv "${D}"/var "${D}"/usr

#  dodir /etc/env.d
#  insXinto /etc/env.d
#  insopts -m 644
#  doins ${FILESDIR}/99qware-env
doenvd "${FILESDIR}"/99qware-env
}

pkg_setup() {
	if [ ! -d "${PORTAGE_CONFIGROOT%/}/etc/portage/qrtools.conf" ] ; then
	  mkdir -p "${PORTAGE_CONFIGROOT%/}/etc/portage/qrtools.conf" ; fi

	echo "" >> /etc/portage/make.conf
	echo
}
