# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
#
# Rev. 20171020, Kai Peter
# The original cronbase package from Gentoo is NOT required for all cron ebuilds,
# e.g. not for fcron. So we need no user/group or '/etc/cron*' folders.

EAPI="7"

#inherit user

DESCRIPTION="dummy package for all cron ebuilds - it does nothing"
HOMEPAGE="http://gentoo.dybdn.es/"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="alpha amd64 arm arm64 hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc x86 ~amd64-fbsd ~sparc-fbsd ~x86-fbsd"
IUSE="-clean"

S=${WORKDIR}

pkg_setup() {
  # remove the stuff from the gentoo package
  if use clean ; then
#	[ -f /usr/sbin/run-crons ] && rm -f /usr/sbin/run-crons
	for CRONDIR in daily hourly weekly monthly
	do
  	  [ -d "/etc/cron.$CRONDIR" ] && rm -rf "/etc/cron.$CRONDIR"
	done
	[ -d "/var/spool/cron" ] && rm -rf "/var/spool/cron"
	userdel cron 2>/dev/null
  fi
}

pkg_compile() {
	einfo "Nothing to compile ..."
}

#pkg_setup() {
#	enewgroup cron 16
#	enewuser cron 16 -1 /var/spool/cron cron
#}

#src_install() {
#	newsbin "${FILESDIR}"/run-crons-${PV} run-crons

#	diropts -m0750
#	keepdir /etc/cron.{hourly,daily,weekly,monthly}

#	keepdir /var/spool/cron/lastrun
#	diropts -m0750 -o root -g cron
#	keepdir /var/spool/cron
#}
