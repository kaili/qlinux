#
# qLinux uses some dedicated programs/tools, so we mask alternatives
#
# qLinux prefers frcon exclusive always
sys-process/anacron
sys-process/bcron
sys-process/dcron
#sys-process/cronie
#sys-process/systemd-cron
#sys-process/vixie-cron


# Kai Peter <email@address> (13 Jul 2019)
# qLinux prefers the 'nail' mailx package, so we mask berkley's one
mail-client/mailx

mail-mta/esmtp
mail-mta/msmtp
mail-mta/ssmtp

#app-admin/metalog

sys-boot/grub-static

# Kai Peter <email@address> (22 Jul 2019)
# overlay packages of the 'gentoo' repository
>mail-client/mailx-0
>virtual/mailx-2

>sys-process/cronbase-1
<sys-process/cronbase-1
